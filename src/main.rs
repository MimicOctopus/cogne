use getopts::Options;
use std::env;
use socket2::{Domain, SockAddr, Socket, Type};
use std::net::ToSocketAddrs;
use std::{thread, time};
use libc::EINPROGRESS;

fn build_address(host: &str, port: &str) -> Result<SockAddr, &'static str> {
    let tmp = port.parse::<u64>().unwrap_or(0);
    if tmp > 0 && tmp <= 65536 {
        match format!("{}:{}", host, port).to_socket_addrs() {
            Ok(mut addr_iter) => match addr_iter.next() {
                Some(addr) => {
                    let res: SockAddr = SockAddr::from(addr);
                    return Ok(res);
                }
                None => return Err("No such host"),
            },
            Err(_) => return Err("Invalid hostname"),
        }
    } else {
        return Err("Port invalid or out of range");
    }
}

fn knock(host: &str, port: &str, udp: bool, verbose: bool) {
    match build_address(host, port) {
        Ok(addr) => {
            if verbose {
                println!("Knocking: {}:{}, {}", host, port, if udp {"udp"} else {"tcp"});
            }
            let t: Type;
            t = if udp { Type::dgram() } else { Type::stream() };
            let socket = Socket::new(Domain::ipv4(), t, None).unwrap();
            if !udp {
                socket.set_nonblocking(true).unwrap();
                let _ = match socket.connect(&addr) {
                    Ok(sock) => sock,
                    Err(error) => match error.raw_os_error() {
                        Some(EINPROGRESS) => (),
                        Some(other_error) => panic!("Problem opening the socket: {:?}", other_error),
                        None => (),
                    },
                };
            } else {
                socket.send_to(b"", &addr).ok();
            }
        }
        Err(_) => println!("Can't knock"),
    }
}

fn print_usage(program: &str, opts: Options){
    let brief = format!("Usage: {} [options] HOST <port[:proto]> [port[:proto]] ...", program);
    print!("{}", opts.usage(&brief));
}

fn main() {
    let version = "0.1.0";
    let args: Vec<String> = env::args().map(|x| x.to_string()).collect();
    let ref program = args[0];

    let mut opts = Options::new();
    opts.optopt("d", "delay", "Milliseconds between ports. Defaults to 250ms", "DELAY");
    opts.optflag("u", "udp", "Make all port hits use UDP instead of TCP");
    opts.optflag("h", "help", "display this help and exit");
    opts.optflag("v", "verbose", "Make output verbose");
    opts.optflag("V", "version", "output version information and exit");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(_) => { print_usage(program, opts); 
                    return;  }
    };
    
    if matches.opt_present("version"){
        println!("{}", version);
        return;
    }

    let delay_millis = matches.opt_get_default("delay", 250).unwrap();
    let sleep_dur = time::Duration::from_millis(delay_millis);

    if matches.free.len() >= 2 {
        let host = &matches.free[0];
        
        for tmp in matches.free[1..].iter() {
            let udp: bool;
            let port;
            if tmp.contains(":") {
                let mut parts = tmp.split(":");
                port = parts.next().unwrap();
                udp = parts.next().unwrap() == "udp";
            } else {
                udp = false || matches.opt_present("udp");
                port = tmp;
            }
            knock(&host, port, udp, matches.opt_present("verbose"));
            thread::sleep(sleep_dur);
        }
    }
    else{
        print_usage(program, opts);
    }
}
